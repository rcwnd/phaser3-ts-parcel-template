# README

**Phaser3** + **TypeScript** + **Parcel.js** template project
with *TSlint* and *Prettier* as bonus!

## pre-commit git hook

Code is automatically formatted by *Prettier* and checked by *TSlint* just before commit.
Those checks & fixes can be run manually like this:
```bash
npm run format # Prettier
npm run lint # TSlint
```

**TODO: make branch with those options removed**

## install
* clone this repository
```bash
git clone git@bitbucket.org:rincewind/phaser3-ts-parcel-template.git
```

* install node packages
```bash
npm install
```

* run project using *Parcel.js*
```bash
npm start
```
Application is now available on http://127.0.0.1:4000/

## deploy
**TODO: deploy**

see _package.json_ for other built-in commands.

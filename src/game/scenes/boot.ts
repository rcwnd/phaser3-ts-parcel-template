import * as Phaser from 'phaser'
import { GameUrl } from '../config/game-config'
import { DefaultScene } from './default'

export class BootScene extends DefaultScene {
  public static readonly BASE_URL = GameUrl

  protected assetText: Phaser.GameObjects.Text
  protected progressBar: Phaser.GameObjects.Graphics
  protected percentText: Phaser.GameObjects.Text
  protected loadingText: Phaser.GameObjects.Text
  protected progressBox: Phaser.GameObjects.Graphics

  constructor() {
    super('boot-scene')
  }

  public loadAssets() {
    this.load.setBaseURL(BootScene.BASE_URL)
    // Load assets here
  }

  public preload() {
    this.createUI()
    this.loadAssets()
    console.log(this.load)
  }

  protected createUI() {
    this.progressBar = this.add.graphics()
    this.progressBox = this.add.graphics()

    this.progressBox.fillStyle(0x222222, 0.8)
    this.progressBox.fillRect(240, 270, 320, 50)

    this.loadingText = this.make.text({
      x: this.scale.width / 2,
      y: this.scale.height / 2 - 50,
      text: 'Loading...',
      style: {
        font: '20px monospace',
        fill: '#ffffff'
      }
    })
    this.loadingText.setOrigin(0.5, 0.5)

    this.percentText = this.make.text({
      x: this.scale.width / 2,
      y: this.scale.height / 2 - 5,
      text: '0%',
      style: {
        font: '18px monospace',
        fill: '#ffffff'
      }
    })
    this.percentText.setOrigin(0.5, 0.5)
    console.log(this.percentText)

    this.assetText = this.make.text({
      x: this.scale.width / 2,
      y: this.scale.height / 2 + 50,
      text: '',
      style: {
        font: '18px monospace',
        fill: '#ffffff'
      }
    })

    this.assetText.setOrigin(0.5, 0.5)
    this.load.on('progress', (value: any) => this.onProgress(value))
    this.load.on('fileprogress', (file: Phaser.Loader.File) =>
      this.onFileProgress(file)
    )
    this.load.on('complete', () => this.onComplete())
  }

  protected onComplete() {
    this.progressBar.destroy()
    this.progressBox.destroy()
    this.loadingText.destroy()
    this.percentText.destroy()
    this.assetText.destroy()
  }

  protected onFileProgress(file: Phaser.Loader.File) {
    this.assetText.setText('Loading asset: ' + file.key)
  }

  protected onProgress(value: any) {
    this.percentText.setText((value * 100).toString() + '%')
    this.progressBar.clear()
    this.progressBar.fillStyle(0xffffff, 1)
    this.progressBar.fillRect(250, 280, 300 * value, 30)
  }
}
